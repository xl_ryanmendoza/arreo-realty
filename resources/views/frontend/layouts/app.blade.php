<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="canonical" href="https://arreorealty.com/" />
    <meta name="description" content="LET ARREO REALTY KNOW WHAT MATTER MOST TO YOU IN A HOME OR OFFICE AND WE’LL FIND YOU WITH PROPERTIES THAT MEET YOUR DEMANDS.">
    <meta name="keywords" content="To acquire by purchase, lease, donation or otherwise, and to own, use, improve, develop, subdivide, sell,mortgage, exchange, lease, develop, and hold for investment or otherwise, real estate of all kinds, whatever improve, manage, or otherwise dispose of buildings, houses, apartments, and other structures of whatever kind, together with their appurtenances." />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Rent to Own Condo in Fort Bonifacio, Taguig | Arreo Realty Exchange" />
    <meta property="og:description" content="Real estate broker selling Rent to Own Condo in Fort Bonifacio, Taguig, Megaworld, Metro Manila Philippines. Best Price Guarantee. Affordable Residences." />
    <meta property="og:url" content="https://arreorealty.com/" />
    <meta property="og:site_name" content="Arreo Realty Exchange Inc." />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Real estate broker selling Rent to Own Condo in Fort Bonifacio, Taguig, Megaworld, Metro Manila Philippines. Best Price Guarantee. Affordable Residences." />
    <meta name="twitter:title" content="Rent to Own Condo in Fort Bonifacio, Taguig | Arreo Realty Exchange" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel Real Estate') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('frontend/css/materialize.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    @yield('styles')

    <link href="{{ asset('frontend/css/styles.css') }}" rel="stylesheet">
    <style media="screen">
      .testimonials .card {
        background: transparent!important;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
      }
      .testimonials .card .card-image img {
        display: inline-block;
        width: 150px;
        height: 100px;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: 50% 50%;
      }
       .deep-orange.darken-4 {
        background-color: #5A3D07!important;
      }
      .deep-orange.darken-2 {
        background-color: #825804!important;
      }

      .deep-orange-text {
        color: #825804!important;
      }

      .dropdown-trigger::before {
        border-left: 2px groove #825804!important;
      }
    </style>
</head>

    <body>

        {{-- MAIN NAVIGATION BAR --}}
        @include('frontend.partials.navbar')

        {{-- SLIDER SECTION --}}
        @if(Request::is('/'))
            @include('frontend.partials.slider')
        @endif

        {{-- SEARCH BAR --}}
        @include('frontend.partials.search')

        {{-- MAIN CONTENT --}}
        <div class="main">
            @yield('content')
        </div>

        {{-- FOOTER --}}
        @include('frontend.partials.footer')


        <!--JavaScript at end of body for optimized loading-->
        {{-- <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script> --}}
        <script type="text/javascript" src="{{ asset('frontend/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('frontend/js/materialize.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        {!! Toastr::message() !!}
        <script>
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    toastr.error('{{ $error }}','Error',{
                        closeButtor: true,
                        progressBar: true
                    });
                @endforeach
            @endif
        </script>

        @yield('scripts')

        <script>
        $(document).ready(function(){
            $('.sidenav').sidenav();

            $('.carousel.carousel-slider').carousel({
                fullWidth: false,
                indicators: true,
            });

            $('.carousel.testimonials').carousel({
                indicators: false,
            });

            var city_list =<?php echo json_encode($citylist);?>;
            $('input.autocomplete').autocomplete({
                data: city_list
            });

            $(".dropdown-trigger").dropdown({
                top: '65px'
            });

            $('.tooltipped').tooltip();

        });
        </script>

    </body>
  </html>
