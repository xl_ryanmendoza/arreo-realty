@extends('backend.layouts.app')

@section('title', 'Create Partners')

@push('styles')


@endpush


@section('content')

    <div class="block-header">
        <a href="{{route('admin.users.index')}}" class="waves-effect waves-light btn btn-danger right m-b-15">
            <i class="material-icons left">arrow_back</i>
            <span>BACK</span>
        </a>
    </div>

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header bg-deep-orange">
                    <h2>CREATE USER</h2>
                </div>
                <div class="body">
                    <form action="{{route('admin.users.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" name="name" class="form-control">
                                <label class="form-label">Name</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" name="username" class="form-control">
                                <label class="form-label">Username</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" name="email" class="form-control">
                                <label class="form-label">Email</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" name="password" class="form-control">
                                <label class="form-label">Password</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" name="password_confirmation" class="form-control">
                                <label class="form-label">Confirm Password</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line {{$errors->has('purpose') ? 'focused error' : ''}}">
                                <label>Select Role</label>
                                <select name="role_id" class="form-control show-tick">
                                    <option value="">-- Please select --</option>
                                    @foreach( $roles as $key => $role)
                                    <option value="{{ $role->id }}" >{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <img src="" id="user-imgsrc" class="img-responsive">
                            <input type="file" name="image" id="user-image-input" style="display:none;">
                            <button type="button" class="btn bg-grey btn-sm waves-effect m-t-15" id="user-image-btn">
                                <i class="material-icons">image</i>
                                <span>UPLOAD IMAGE</span>
                            </button>
                        </div>

                        <button type="submit" class="btn btn-deep-orange btn-lg m-t-15 waves-effect">
                            <i class="material-icons">save</i>
                            <span>SAVE</span>
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('scripts')

<script>
    $(function(){
        function showImage(fileInput,imgID){
            if (fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e){
                    $(imgID).attr('src',e.target.result);
                    $(imgID).attr('alt',fileInput.files[0].name);
                }
                reader.readAsDataURL(fileInput.files[0]);
            }
        }
        $('#user-image-btn').on('click', function(){
            $('#user-image-input').click();
        });
        $('#user-image-input').on('change', function(){
            showImage(this, '#user-imgsrc');
        });
    })
</script>

@endpush
