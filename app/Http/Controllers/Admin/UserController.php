<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\User;
use App\Role;
use Hash;
use Carbon\Carbon;
use Toastr;

class UserController extends Controller
{
    public function index()
    {
        $users = User::latest()->get();

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::latest()->get();
        return view('admin.users.create', compact('roles'));
    }

    public function store(Request $request)
    {
      $request->validate([
          'name' => 'required',
          'username' => 'required',
          'image' => 'mimes:jpeg,jpg,png',
          'email' => 'required|max:200',
          'role_id' => 'required|max:2',
          'password' => 'required|confirmed|max:16',
          'password_confirmation'=>'sometimes|required_with:password',
      ]);

      $image = $request->file('image');
      $slug  = str_slug($request->name);

      if(isset($image)){
          $currentDate = Carbon::now()->toDateString();
          $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

          if(!Storage::disk('public')->exists('users')){
              Storage::disk('public')->makeDirectory('users');
          }
          $user = Image::make($image)->resize(160, 160)->stream();
          Storage::disk('public')->put('users/'.$imagename, $user);
      }else{
          $imagename = 'default.png';
      }

      $user = new User();
      $user->name = $request->name;
      $user->username = $request->username;
      $user->email = $request->email;
      $user->password = Hash::make($request->password);
      $user->role_id = $request->role_id;
      $user->image = $imagename;
      $user->save();

      Toastr::success('message', 'User created successfully.');
      return redirect()->route('admin.users.index');
    }


    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::latest()->get();

        return view('admin.users.edit', compact('user','roles'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'username' => 'required',
            'image' => 'mimes:jpeg,jpg,png',
            'email' => 'required|max:60',
            'role_id' => 'required|max:2',
            'password' => 'confirmed|max:16'
        ]);

        $image = $request->file('image');
        $slug  = str_slug($request->title);
        $user = User::find($id);

        if(isset($image)){
            $currentDate = Carbon::now()->toDateString();
            $imagename = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            if(!Storage::disk('public')->exists('users')){
                Storage::disk('public')->makeDirectory('users');
            }
            if(Storage::disk('public')->exists('users/'.$user->image)){
                Storage::disk('public')->delete('users/'.$user->image);
            }
            $userimg = Image::make($image)->resize(160, 160)->stream();
            Storage::disk('public')->put('users/'.$imagename, $userimg);
        }else{
            $imagename = $user->image;
        }

        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->image = $imagename;
        $user->role_id = $request->role_id;
        if ($request->password) {
          $user->password = Hash::make($request->password);
        }
        $user->save();

        Toastr::success('message', 'User updated successfully.');
        return redirect()->route('admin.users.index');
    }


    public function destroy($id)
    {
        $user = User::find($id);

        if(Storage::disk('public')->exists('user/'.$user->image)){
            Storage::disk('public')->delete('user/'.$user->image);
        }

        $user->delete();

        Toastr::success('message', 'User deleted successfully.');
        return back();
    }
}
